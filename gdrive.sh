#!/bin/bash
#token="ya29.a0AfH6SMBHDvLFeW3tTACdwgHcBbaPBcg6gWvVjHY4og8uu_PLrx9sKpl13K4h1sgncorSOQwBuwDlBZWBO9Nga1f8znaoAhQZmQsE08o8FWdmworTeVnmnZz3vHs3oMusnu_Ma9p5gqwEVg8loDYKb9xZIpWF74-JJSM"
path=$1 #e.g ~/gdrive.sh /home/calvin/abc. 
folder=$2 
tokenFile=$3
desc=$4  
#always get a refreshed Token
~/doc/bash/google_oauth.sh $tokenFile 3


#color
GREEN='\033[0;32m'
BLUE='\033[0;34m'
RED='\033[0;31m'
NC='\033[0m' # No Color


API="AIzaSyCRZeZlSHF3qYsDy8BTqT1pUSgemJuK05M"
access_code=$(awk 'NR==3' ~/doc/bash/$tokenFile.txt)
q="Access Token:"
q=$((${#q}+1))
access_code=${access_code:q}
access_code=${access_code::-1}
echo $access_code

printf "File Path:"
if [ "$tokenFile" = "hkbu" ];then
printf "\nGoogle Drive:${BLUE}$tokenFile${NC}\n\n"
fi

if [ "$tokenFile" = "airgolden" ];then
printf "\nGoogle Drive:${GREEN}$tokenFile${NC}\n\n"
fi



if [ -z "$path" ];then
read path
else
  path=${path//[~]/"/home/calvin"}
fi
name=$(basename $path)
ftype=$(mimetype $path)
echo $name
ftypeLen=${#ftype}
nameLen=$((${#path}+1))
mime=${ftype:nameLen:ftypeLen}
#echo $ftypeLen
#echo $nameLen
echo $mime
#personal folder 1
#folderId="1qPdOa5aWmMs1RAmGRCpjxnsyO3BsEgNv" #personal 1/

printf "#1 HKBU OPS/Quantr/Spfx/source_code/ \n"
printf "#2 Strava TCX Files backup/Cycling/strava/\n"
printf "#:" 
if [-z "$folder" ];then
read folder

fi
if [ $folder -eq 1 ];then
folderId="13PnwGyuBbanlIYFaFrNleWxMdvL2_G2z" #hkbu, ops/quantr/spfx/source_code/
fi

if [ $folder -eq 2 ];then
folderId="14foUNmnFBdmRgc-WFWViG2p_DqtHZqCT" #hkbu, backup/Cycling/strava/
fi

if [ $folder -eq 3 ];then
folderId="1Fkf4ClSnmdIG1x4XP5fqzXlYJLpHXKy2" #airgolden, backup/Cycling/strava
fi

if [ $folder -eq 4 ];then
folderId="1JKh5pn39HLw3itnd_7Ok2iBmH7UoBqsn" #airgolden, backup/google_drive/HKBU/OPS/Quantr/spfx/source_code
fi


 #airgolden

echo $folderId

#upload file to Google Drive
response=$(curl -X POST  \
    -H "Authorization: Bearer $access_code"\
  --header 'Accept: application/json' \
  --header "Content-Type: $mime" \
  "https://www.googleapis.com/upload/drive/v3/files?uploadType=multipart" \
  --data-binary "@$path") 
printf " Result:$response"
fileId=$(echo $response |jq '.id')
fileIdLen=${#fileId}
fileId=${fileId:1:fileIdLen}
fileId=${fileId::-1}
echo $fileId
#rename and Relocate the Uploaded File in Google Drive
printf "\nRenaming and Moving to Desired Folder...\n"
rename=$(curl --request PATCH \
  "https://www.googleapis.com/drive/v3/files/$fileId?addParents=$folderId&removeParents=root&key=$API" \
  --header "Authorization: Bearer $access_code" \
  --header 'Accept: application/json' \
  --header 'Content-Type: application/json' \
  --data "{'name':'$name', 'description':'$desc'}" \
  --compressed)
printf "Rename:$rename"
error=$(echo $rename|jq '.code')
results=$(echo $rename|jq '.id')
echo $error
if [ "$error" -gt 400 && "$error" != null ];then
#refresh token
 ~/doc/bash/google_oauth.sh $tokenFile 3
fi
echo $results
if [ ! -z "$results" ];then
	printf "[${GREEN}OK${NC}]"
	printf "\nUpload Completed, Check for errors!\n"
else
	printf "[${RED}FAIL${NC}]"
echo $error $rename

fi


read wait
exit 0;
