#!/bin/bash
cd $1
prompt="#?:"
options=( $(ls -1 -d */) )
PS3="----------------------------------------"$'\n'
PS3="${PS3}<..> back"$'\n'
PS3="${PS3}<.> OPEN FOLDER <t> Terminal"$'\n'
PS3="${PS3}<q> Quit"$'\n'
PS3="${PS3} $prompt"
while :; do
clear
echo "----------------------------------------"
printf "DIR NAV\n"
printf "PWD:"
pwd
echo "----------------------------------------"
	select opt in "${options[@]}" "Quit" ; do
		echo "$REPLY"
	    if (( REPLY == 1 + ${#options[@]} )) ; then
		exit
		elif [ "$REPLY" == ".." ]; then
				cd .. 
				options=( $(ls -d */) )				
				break 
	    elif [ "$REPLY" == "." ]; then
		xdg-open .
		break
	    elif [ "$REPLY" == "t" ]; then
		gnome-terminal .
		break
	    elif (( REPLY > 0 && REPLY <= ${#options[@]} )) ; then
		echo "----------------------------------------"
		printf "DIR NAV\n"
		cd $opt
		printf "PWD:"
		pwd
		echo "----------------------------------------"
		options=( $(ls -d */) )
		break
	   elif (( REPLY == 'q' )) ; then
		   exit 0
			
	   else
		echo "Invalid option. Try another one."
	    fi
	done
done

