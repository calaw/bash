#!/bin/bash
#
yo @microsoft/sharepoint \
--solution-name "a" \
--framework "react" \
--component-type "webpart" \
--component-name "a" \
--component-description "a" \
--environment "spo" \
--skip-feature-deployment \
--is-domain-isolated 
cd ~/spfx/a
gulp serve
