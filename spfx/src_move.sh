#!/bin/bash
#move dev/src to testing/src
GREEN='\033[0;32m'
BLUE='\034[0;32m'
RED='\033[0;31m'
NC='\033[0m' # No Color

function menu() {

clear
echo Move dev/src to testing src
printf "deploy=  testing->dev \n"
printf "!deploy= dev->testing \n"
echo ------------------------------------------------------
printf "<.. back/EXIT>\n"
cd ~/spfx/
options=( $(ls -1 -d */) )
while :; do
clear
echo "$folder"
echo "----------------------------------------"
printf "DIR NAV\n"
printf "PWD:"
pwd
echo "<.> Select"
echo "----------------------------------------"
	select opt in "${options[@]}" "Quit" ; 
	do
	 if (( REPLY == 1 + ${#options[@]} )) ; then
		folder=".."				
		break 2
	elif [ "$REPLY" == ".." ]; then
				cd .. 
				options=( $(ls -d */) )				
				break 
	elif [ "$REPLY" == "." ]; then

		break 2
	elif (( REPLY > 0 && REPLY <= ${#options[@]} )) ; then
		echo "----------------------------------------"
		printf "DIR NAV\n"
		printf "PWD:"
		pwd
		echo "<.> Select"
		echo "----------------------------------------"
		cd $opt	
		folder=$opt	
		options=( $(ls -d */) )
		break
	  elif (( REPLY == 'q' )) ; then
		   folder=".."
		   break 2
			
	  else
		echo "Invalid option. Try another one."
	    fi
	done
done
echo "---------------------------------------"
echo $folder
if [ "$folder" = ".." ]; then
	exit 0
fi
read -p "testing/~spfx <1/0>?" dev
if [ "$dev" = ".." ]; then
	exit 0
fi
read -p "Deploy <1/0>?" deploy
if [ "$deploy" = ".." ]; then
	exit 0
fi
#deploy means testing -> dev
#!deploy meanss dev->testing
if [ "$folder" = ".." ]; then
	exit 0
fi

if [ "$deploy" = ".." ]; then
	exit 0
fi

if [ "$dev" == "1" ]; then
	#dev
	echo testing
	if [ "$deploy" == "1" ]; then
	echo deploy
		
		mkdir ~/spfx/$folder
		if command=$(cp -rfv ~/spfx/testing/$folder/src/ ~/spfx/$folder/); then
		    cp -rfv ~/spfx/testing/$folder/*.json ~/spfx/$folder/
			cp -rfv ~/spfx/testing/$folder/*.js ~/spfx/$folder/
			printf "testing->dev\n"
			printf "$command\n"
			printf "${GREEN} Completed!\n"
			printf "${NC}"
				
		else 
				
			printf "${RED}"
			printf "Error: $command\n"
			
			
			
		fi
			printf "${NC}"
		
		
	else
		printf "dev->testing\n"
		mkdir ~/spfx/testing/$folder

		if command=$(cp -rfv ~/spfx/$folder/src/ ~/spfx/testing/$folder/); then
			 cp -rfv ~/spfx/$folder/*.json ~/spfx/testing/$folder/
			 cp -rfv ~/spfx/$folder/*.js ~/spfx/testing/$folder/

		   
			printf "$command\n"
			printf "${GREEN} Completed!\n"
			printf "${NC}"
			
		else 
			printf "${RED}"
			printf "Error: $command\n"
			printf "${NC}"
			
			
		fi
	fi
else	
	#spfx root
	printf "spfx root"
	if [ "$deploy" == "1" ]; then
		mkdir ~/spfx/$folder
		mkdir ~/spfx/$folder/src
		
		if command=$(cp -rfv ~/spfx/testing/$folder/src/ ~/spfx/$folder/); then
		cp -rfv ~/spfx/testing/$folder/*.json ~/spfx/$folder/
		cp -rfv ~/spfx/testing/$folder/*.js ~/spfx/$folder/
		printf "testing->~/spfx\n"	
		printf "$command\n"

			printf "${GREEN} Completed!\n"
			printf "${NC}"
				
		else 
			printf "${RED}"
			printf "Error: $command\n"
			
			
			
		fi
			printf "${NC}"
		
		
	else     
                printf "\n$folder"
		printf "\nspfx->~/testing\n"
		rm -rfv ~/spfx/testing/$folder
		mkdir ~/spfx/testing/$folder
			

		if command=$(cp -rfv ~/spfx/$folder/src/ ~/spfx/testing/$folder/ ); then
			cp -rfv ~/spfx/$folder/*.json ~/spfx/testing/$folder/
			cp -rfv ~/spfx/$folder/*.js ~/spfx/testing/$folder/
			printf "$command\n"
			printf "${GREEN} Completed!\n"
			printf "${NC}"
			
		else 
			printf "${RED}"
			printf "Error: $command\n"
			printf "${NC}"
			
			
		fi
	fi
fi




read -p "Continue? <Enter=YES/..=QUIT>" folder
}

while :; do
	menu
if [ "$folder" = ".." ]; then
	exit 0
fi
	done
