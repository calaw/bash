#!/bin/bash
# developed by CALAW 2020-03-02
clear
function menu(){
date
printf "******MENU*****\n"
printf "1. INIT LEVEL\n"
printf "2. update.sh\n"
printf "3. SPFx yo @microsoft/sharepoint \n"
printf "4. git clone \n"
printf "5. git push \n"
printf "6. dev ->testing src \n"
printf "7. Reinstall NPM packages \n"
printf "8. Dir NAV\n"
printf "i. systemctl suspend -i\n"
printf "0. RAW Command \n"
printf "*. 1.sh git pull \n"
printf "9  GoogleDrive OAUTH \n"
printf "10 GDrive Upload #path #folder #tokenFile\n"
printf "11 Strava Upload\n"
printf "***************\n"

}

function runlvl(){
while :; do
clear
date
printf "******INIT RUN LEVEL*****\n"
printf "INIT 0\n"
printf "INIT 6\n"
printf "<.. back>\n"
printf "INIT:"
read lvl

if [ "$lvl" -eq 6 ]; then
	sudo init 6
	
fi

if [ "$lvl" -eq 0 ]; then
	 sudo init 0
	
fi


if [ "$lvl" = ".." ]; then
	break
	menu
fi

done
}


	while :; do
	clear
	menu;
	printf "\n"
	printf "q=quit\n"
	printf "Enter your selection(s) and press <Enter> "
	
	read input
	if [ "$input" -eq 0 ];then 
		clear
		printf "RAW Command Mode\n"
		printf "<.. back>\n"
		printf "<Enter>To Clear Screen\n"	
		printf "$USER@$HOSTNAME:~$ "
		read input		
		 while [ "$input" != ".." ] ; do
		clear
		printf "RAW Command Mode\n"
		printf "<.. back>\n"
		printf "<Enter>To Clear Screen\n"
		printf "$USER@$HOSTNAME:~$ "
		printf "\n"
		$input
			
		    read input
		   if [ "$input" = ".." ]; then
		        break 
		   fi
		 done
		
	fi

	if [ "$input" -eq 3 ];then 
		gnome-terminal --working-directory=/home/calvin/doc/bash/spfx/
	fi
	
	if [ "$input" -eq 4 ];then 
	 	gnome-terminal -- ~/doc/bash/spfx/clone.sh
	fi

	if [ "$input" -eq 5 ];then 
		~/doc/bash/spfx/git.sh
	fi
	if [ "$input" -eq 6 ];then 
		~/doc/bash/spfx/src_move.sh
	fi

	if [ "$input" -eq 7 ];then 
		gnome-terminal -- ~/doc/bash/spfx/npm_i.sh
	fi

	if [ "$input" -eq 8 ];then 
		gnome-terminal -- ~/doc/bash/dir.sh ~/spfx/
	fi
	if [ "$input" -eq 9 ];then 
		gnome-terminal -- ~/doc/bash/google_oauth.sh
	fi

	if [ "$input" -eq 10 ];then 
		gnome-terminal -- ~/doc/bash/gdrive.sh
	fi
	if [ "$input" -eq 11 ];then 
		gnome-terminal -- ~/doc/bash/strava.sh
	fi	

	if [ "$input" = "*" ];then 
		gnome-terminal -- ~/doc/bash/1.sh && exit 0;

	fi

	if [ "$input" = 'i' ];then 
		~/doc/bash/sys/i.sh
	fi
	
	if [ "$input" -eq 1 ];then 
		runlvl 
	fi
	
	if [ "$input" -eq 2 ];then 
		~/doc/bash/sys/update.sh
		printf "\n******Press Enter To Continue"
		read input
		if [ "$input" = "" ]; then
		clear	
		menu
		fi
		
	fi
	
	if [ "$input" = "q" ]; 
	then	
		clear
		exit 0
	fi
	done

exit 0


