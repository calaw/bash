#!/bin/bash
# developed by CALAW 2020-05-24
clear
printf "1. Zip all Tcx files\n"
printf "2. Upload to HKBU strava folder"
printf "\n"
cd ~/dl/
zip -ru ~/dl/strava.zip *.tcx
zip -ru ~/dl/strava.zip *.fit
~/doc/bash/gdrive.sh ~/dl/strava.zip 2 hkbu&&\
~/doc/bash/gdrive.sh ~/dl/strava.zip 3 airgolden&&\
rm ~/dl/strava.zip
printf "Upload OK.....\n"
exit 0
